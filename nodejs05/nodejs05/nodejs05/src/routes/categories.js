import express from 'express';

import CategoryController from "../controllers/category";
import app from "../app";

const router = express.Router();
const category_controller = new CategoryController();

router
    .get('/', (req, res) => {
        res.render('pages/homepage.pug');
    })
    // .get('/create', category_controller.getCreateForm)
    // .get('/create', category_controller.getAll)
    // .get('/create', category_controller.getCreateForm)
    .get('/create',  (req, res) => {category_controller.getCreateForm(req,res)})

    // .get('/create', (req, res) => {
    //     res.render('pages/create.pug',category_controller.getCreateForm);
    // })

    .get('/update', (req, res) => {category_controller.getUpdateForm(req,res)})
    .get('/delete', (req, res) => {category_controller.getDeleteForm(req,res)})

    .post('/create', category_controller.validate(), category_controller.create)
    .post('/update', category_controller.validate(), category_controller.update)
    .post('/delete', category_controller.delete);

export default router
