import checkAPIs from 'express-validator';
const { check } = checkAPIs;
import BaseController from "./base";
import AirportDetail from '../models/airportDetail'

// const CATEGORY_PREFIX = '/categories';

// Create a class named "CategoryController" which will inherit the methods from the "BaseController" class,
// by using the extends keyword.
// By calling the super() method in the constructor method, we call the parent's constructor method and
// get access to the parent's properties and methods:

class CategoryController extends BaseController {
    constructor() {
        super(AirportDetail);

        this.getForms = this.getForms.bind(this);
        this.validate = this.validate.bind(this);
    }

    validate() {
        return [
            check('airport_code')
                .exists().withMessage('Field category_name must exist')
                .isString().withMessage('Category name must be of the string type')
                .trim().isLength({ min: 3 }).withMessage('Category name must not be at least 3 characters long'),
            check('airport_phonenumber')
                .exists().withMessage('Field category_name must exist')
                .isString().withMessage('Category name must be of the string type')
                .trim().isLength({ min: 3 }).withMessage('Category name must not be at least 3 characters long')
        ]
    }

    async getForms(req, res) {
        res.render('pages/create', { categories: await this._model.find().lean() });
    }

    //lean() makes queries faster and less memory intensive, but the result documents are plain old JavaScript objects (POJOs),
    // not Mongoose documents.

    // By default, Mongoose queries return an instance of the Mongoose Document class.
    // Documents are much heavier than vanilla JavaScript objects, because they have a lot
    // of internal state for change tracking. Enabling the lean option tells Mongoose to skip instantiating a
    // full Mongoose document and just give you the POJO.

    async getCreateForm(req, res) {
        res.render('pages/create', {categories: await this._model.find().lean()});
    }
    async getUpdateForm(req, res) {
        res.render('pages/update', {categories: await this._model.find().lean()});
    }
    async getDeleteForm(req, res) {
        res.render('pages/delete', {categories: await this._model.find().lean()});
    }
    async getAll(req, res) {
        try {
            debugger;
            let categories = await this._model.find().lean();

            return res.json(categories);
        } catch (e) {
            this.sendError(res, e);
        }
    }

    async update(req, res) {
        try {
            let data = {
                airport_code:req.body.airport_code,
                airport_phonenumber: req.body.airport_phonenumber
            };

            let obj = await this._model.findByIdAndUpdate(req.body.id, data, {new: true});
            if (obj === null) throw new ReferenceError("Object with this id does not exist");
            res.redirect('/update');
            return res.json(obj);
        } catch (e) {
            this.sendError(res, e);
        }
    }
}

export default CategoryController
