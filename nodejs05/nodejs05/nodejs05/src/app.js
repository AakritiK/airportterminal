//IMPORTING THE TOOLS YOU WILL BE USING
import path from 'path';
import express from 'express';
import mongoose from "mongoose";
import bodyParser from 'body-parser';
import categoriesRouter, {  } from './routes/categories';

mongoose.connect('mongodb://localhost:27017/AirplaneManagement');

let app = express();

app.set('views', path.join(path.resolve(), 'src', 'views'));
app.set('view engine', 'pug');
// app.get('/', (req, res) => {
//     res.render('pages/homepage.pug');
// });

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/static', express.static(path.join(path.resolve(), 'src', 'public')));

app.use( categoriesRouter);

export default app;
