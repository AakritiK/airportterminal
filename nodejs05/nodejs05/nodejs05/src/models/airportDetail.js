import mongoose from 'mongoose';

const airportSchema = new mongoose.Schema({
    airport_code: {
        type: String,
        required: true
    },
    airport_phonenumber: {
        type: String,
        required: true
    }
});

const AirportDetail = mongoose.model('Airport', airportSchema, 'airport');

export default AirportDetail